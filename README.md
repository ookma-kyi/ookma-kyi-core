# Ookma-Kyi

> ### A Laravel implementation of the now defunct Battlemail game

Battlemail was an email game originally written by Paul Gouge. The concept is
fairly simple: Players pick 6 attack and 6 defense moves which are sent to their
opponent. The opponent then picks 6 attack and 6 defense moves. Once submitted,
both players are then treated to a display of martial arts based on the moves
both players picked.

----------

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)

Alternative installation is possible without local dependencies relying on [Docker](#docker). 

Clone the repository

    git clone git@bitbucket.org:ookma-kyi/ookma-kyi-core.git

Switch to the repo folder

    cd ookma-kyi-core

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@bitbucket.org:ookma-kyi/ookma-kyi-core.git
    cd ookma-kyi-core
    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

**Populate the database with seed data with relationships which includes users, characters and matches. This can help you to quickly start testing the frontend and start using it with ready content.**

Open the DataSeeder and set the property values as per your requirement

    database/seeds/DataSeeder.php

Run the database seeder and you're done

    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh
    
## Docker

To install with [Docker](https://www.docker.com), run following commands:

```
git clone git@bitbucket.org:ookma-kyi/ookma-kyi-core.git
cd ookma-kyi-core
cp .env.example.docker .env
docker run -v $(pwd):/app composer install
cd ./docker
docker-compose up -d
docker-compose exec php php artisan key:generate
docker-compose exec php php artisan migrate
docker-compose exec php php artisan db:seed
docker-compose exec php php artisan serve --host=0.0.0.0
```

The app can be accessed at [http://localhost:8000/](http://localhost:8000/).

----------

# Code overview

## Dependencies


## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers` - Contains all the controllers
- `app/Http/Middleware` - Contains all the auth middleware
- `config` - Contains all the application configuration files
- `database/factories` - Contains the model factory for all the models
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `routes` - Contains all the api routes defined in api.php file
- `tests` - Contains all the application tests

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

## Thanks
[![Blackfire.io](https://i.imgur.com/xAI9rHb.png)](https://www.blackfire.io/)  
[![Concord](https://i.imgur.com/59ThxsS.png)](https://www.concord.tech/)  
[![HelioHost](https://i.imgur.com/bRQc9a9.png)](https://heliohost.org/)  
[![PHPStorm Subscription provided by JetBrains](https://i.imgur.com/OFsStni.png)](https://www.jetbrains.com/)
