<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Throwable;

class RemoveRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'useradmin:removerole {role} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes the specified role from the given user';

    /**
     * Execute the console command to remove specified role from the given user.
     * @throws Throwable
     */
    public function handle(): void
    {
        // get the id of the requested role
        $role = Role::where('name', $this->argument('role'))->first(['id']);

        // if the role doesn't exist
        if (!$role) {
            // fail with an error message
            $this->fail("Role {$this->argument('role')} doesn't exist!");
        }

        // get the user's id from the database
        $user = User::where('email', $this->argument('email'))->first(['id']);

        // if the user doesn't exist
        if (!$user) {
            // fail with an error message
            $this->fail("User doesn't exist!");
        }

        // if the user and role exist, find if the user has the role
        $user_has_role = $user->hasRole($this->argument('role'));

        // if the user doesn't have the role
        if(!$user_has_role) {
            // fail with an error message
            $this->fail("User doesn't have the {$this->argument('role')} role");
        }

        // otherwise remove the requested role from the user
        $user->removeRole($this->argument('role'));

        // and display a success message
        echo "{$this->argument('role')} role has been removed from user";
    }
}
