<?php

namespace App\Enumerations;

// enumerations for determining which player is using the move
enum MoveActor: int
{
    /** the Challenger is using the move */
    case CHALLENGER = 0;

    /** the Opponent is using the move */
    case OPPONENT = 1;
}
