<?php

namespace App\Enumerations;

// enumerations for the move type
enum MoveType: int
{
    /** the move is an offensive one */
    case OFFENSIVE = 0;

    /** the move is an defensive one */
    case DEFENSIVE = 1;
}
