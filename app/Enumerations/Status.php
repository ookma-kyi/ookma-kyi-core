<?php

namespace App\Enumerations;

// enumerations for the match status
enum Status: int
{
    /** one of the users is suspected of cheating */
    case CHEATER = -4;

    /** an unknown error has occurred */
    case ERROR = -3;

    /** the match was declined by the opponent */
    case DECLINED = -2;

    /** the match was cancelled by the challenger */
    case CANCELLED = -1;

    /** the match is open is awaiting a response from the opponent */
    case OPEN = 0;

    /** the match has been completed */
    case COMPLETED = 1;
}
