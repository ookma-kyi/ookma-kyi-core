<?php

namespace App\Filament\Resources;

use App\Enumerations\Status;
use App\Filament\Resources\DuelResource\Pages;
use App\Filament\Resources\DuelResource\RelationManagers;
use App\Models\Duel;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class DuelResource extends Resource
{
    protected static ?string $model = Duel::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('challenger_id')
                    ->relationship('challenger', 'name')
                    ->required(),
                Forms\Components\Select::make('opponent_id')
                    ->relationship('opponent', 'name')
                    ->required(),
                Forms\Components\Select::make('status')
                    ->options([
                        Status::COMPLETED->value => Status::COMPLETED->name,
                        Status::OPEN->value => Status::OPEN->name,
                        Status::CANCELLED->value => Status::CANCELLED->name,
                        Status::DECLINED->value => Status::DECLINED->name,
                        Status::ERROR->value => Status::ERROR->name,
                        Status::CHEATER->value => Status::CHEATER->name,
                    ])
                    ->required()
                    ->default(0),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('challenger.name')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('opponent.name')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('status')
                    ->formatStateUsing(function ($state) {
                        // Define the mapping
                        $statusLabels = [
                            Status::OPEN->value => 'OPEN',
                            Status::COMPLETED->value => 'COMPLETED',
                            Status::CANCELLED->value => 'CANCELLED',
                            Status::DECLINED->value => 'DECLINED',
                            Status::ERROR->value => 'ERROR',
                            Status::CHEATER->value => 'CHEATER',
                        ];

                        // Return the corresponding label or a default value
                        return $statusLabels[$state] ?? 'Unknown';
                    })
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDuels::route('/'),
            'create' => Pages\CreateDuel::route('/create'),
            'edit' => Pages\EditDuel::route('/{record}/edit'),
        ];
    }
}
