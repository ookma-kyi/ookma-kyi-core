<?php

namespace App\Filament\Resources;

use App\Enumerations\MoveType;
use App\Filament\Resources\MoveResource\Pages;
use App\Filament\Resources\MoveResource\RelationManagers;
use App\Models\Move;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class MoveResource extends Resource
{
    protected static ?string $model = Move::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('order')
                    ->required()
                    ->numeric(),
                Forms\Components\Select::make('move_type')
                    ->options([
                        MoveType::OFFENSIVE->value => MoveType::OFFENSIVE->name,
                        MoveType::DEFENSIVE->value => MoveType::DEFENSIVE->name,
                    ])
                    ->required()
                    ->default(0),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('order')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('move_type')
                    ->formatStateUsing(function ($state) {
                        // Define the mapping
                        $moveTypeLabels = [
                            MoveType::OFFENSIVE->value => 'OFFENSIVE',
                            MoveType::DEFENSIVE->value => 'DEFENSIVE',
                        ];

                        // Return the corresponding label or a default value
                        return $moveTypeLabels[$state] ?? 'Unknown';
                    })
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMoves::route('/'),
            'create' => Pages\CreateMove::route('/create'),
            'edit' => Pages\EditMove::route('/{record}/edit'),
        ];
    }
}
