<?php

namespace App\Filament\Resources\MoveResource\Pages;

use App\Filament\Resources\MoveResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateMove extends CreateRecord
{
    protected static string $resource = MoveResource::class;
}
