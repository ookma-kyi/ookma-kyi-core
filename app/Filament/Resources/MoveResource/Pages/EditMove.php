<?php

namespace App\Filament\Resources\MoveResource\Pages;

use App\Filament\Resources\MoveResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMove extends EditRecord
{
    protected static string $resource = MoveResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
