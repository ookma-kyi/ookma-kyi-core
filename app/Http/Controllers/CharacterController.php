<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCharacterRequest;
use App\Http\Resources\CharacterResource;
use App\Models\Rank;
use App\Models\Character;
use Inertia\Response;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class CharacterController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        // Authorize the resource based on the model's policies
        $this->authorizeResource(Character::class);
    }

    /**
     * Display a listing of the user's character resources.
     *
     * @return Response inertia response
     */
    public function index(): Response
    {
        // get the user's characters
        $characters = Character::where('user_id', auth()->user()->id)
            ->get(['id', 'name', 'active']);

        return inertia('Characters/Index', [
            'characters' => fn() => CharacterResource::collection($characters)
        ]);
    }

    /**
     * Show the form for creating a new character resource.
     *
     * @return Response inertia response
     */
    public function create(): Response
    {
        // if the play is already at max characters
        if ($this->countCharacter(auth()->user()->id) >= config("characters.max_allowed")) {

            // display an error instead
            return inertia('Characters/MaxCharacters');
        }

        // show the create character form
        return inertia('Characters/Create');
    }

    /**
     * Store a user's newly created character resource in storage.
     *
     * @param StoreCharacterRequest $request user submitted form response
     *
     * @return Response|RedirectResponse redirect response on successful creation,
     * otherwise inertia response on max characters reached
     */
    public function store(StoreCharacterRequest $request): Response|RedirectResponse
    {
        // get the current player's characters count
        $count = $this->countCharacter(auth()->user()->id);

        // if the play is already at max characters
        if ($count >= config("characters.max_allowed")) {

            // display an error instead
            return inertia('Characters/MaxCharacters');
        }

        // retrieve only the name and active status from the request
        $validated = $request->safe()->only(['name', 'active']);

        // if the player already has a character
        if($count) {

            // if the active checkbox was ticked
            if($validated['active']) {

                // get the player's active character if any
                $character = Character::where('user_id', auth()->user()->id)
                    ->where('active', 1)
                    ->first(['id', 'active']);

                // set the current one as not active
                $character->update(['active' => 0]);

                // and save the changes to the database
                $character->save();
            }
            // otherwise this is the player's 1st character
        } else {
            // so mark the character as active
            $validated['active'] = 1;
        }

        // get the first rank
        $rank = Rank::where('min_xp', '<=', 1)
            ->first('id');

        // and create the players character
        Character::create([
            'name' => $validated['name'],
            'user_id' => auth()->user()->id,
            'rank_id' => $rank->id,
            'active' => $validated['active'],

        ]);

        // then redirect to the characters index
        return redirect('characters');
    }

    /**
     * Display the specified character resource.
     *
     * @param Character $character character to show
     *
     * @return Response inertia response
     */
    public function show(Character $character): Response
    {
        //lazy load the rank relationship
        $character->loadMissing('rank:id,name,image');

        // display the character's status
        return inertia('Characters/Show', [
            'user' => auth()->user(),
            'character' => fn() => CharacterResource::make($character)
        ]);
    }

    /**
     * Show the form for editing the specified resource. (unused)
     */
    public function edit(Character $character)
    {
        //
    }

    /**
     * Modify the active status of the player's character resource in storage.
     *
     * @param Character $character character to update active status
     *
     * @return RedirectResponse|JsonResponse redirect response on character already active,
     * otherwise json response on success
     */
    public function update(Character $character): RedirectResponse|JsonResponse
    {
        // if the character is already the active
        if ($character->active) {

            // then redirect to the characters index
            return redirect('characters');
        }

        // get the player's active character if any
        $active_character = Character::where('user_id', auth()->user()->id)
            ->where('active', 1)
            ->first(['id', 'active']);

        // if the play has an active character make it inactive
        $active_character?->update(['active' => 0]);

        // set the selected character as the active one
        $character->update(['active' => 1]);

        // then return ok HTTP Status
        return response()->json(['ok' => 'ok']);
    }

    /**
     * Remove the specified character from storage.
     *
     * @param Character $character character to delete
     *
     * @return RedirectResponse|JsonResponse redirect response if character is active,
     * otherwise json response on success
     */
    public function destroy(Character $character): RedirectResponse|JsonResponse
    {
        // if the character is the active one
        if ($character->active) {
            return redirect('characters');
        }

        // delete the character
        $character->delete();

        // then return ok HTTP Status
        return response()->json(['ok' => 'ok']);
    }

    /**
     * Returns the total number of characters the user has
     * @param int $id user id
     * @return int number of characters user has
     */
    private function countCharacter(int $id) : int
    {
        // return the players character count
        return Character::select('id')->where('user_id', $id)->count();
    }
}
