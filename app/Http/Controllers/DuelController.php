<?php

namespace App\Http\Controllers;

use App\Enumerations\MoveActor;
use App\Enumerations\MoveType;
use App\Enumerations\Status;
use App\Http\Requests\StoreDuelRequest;
use App\Http\Resources\DuelResource;
use App\Http\Resources\MoveResource;
use App\Models\Character;
use App\Models\Duel;
use App\Models\DuelAction;
use App\Models\Move;
use Illuminate\Http\Request;
use Inertia\Response;

class DuelController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        // Authorize the resource based on the model's policies
        $this->authorizeResource(Duel::class);
    }

    /**
     * Display a listing of the user's characters' duel resources.
     *
     * @return Response inertia response
     */
    public function index(): Response
    {
        // get the current player's characters count
        $count = $this->countActiveCharacters(auth()->user()->id);

        // if the player doesn't have any characters
        if ($count === 0) {

            // display an error instead
            return inertia('Duels/NoActiveCharacters');
        }

        // get the user's current active character
        $active_character = Character::where('user_id', auth()->user()->id)
        ->where('active', true)
        ->value('id');

        // get the character's home duels
        $home_duels = Duel::with('opponent:id,name')
            ->where('challenger_id', $active_character)
            ->where('status', Status::OPEN)
            ->paginate(5, ['opponent_id']);

        // get the character's away duels
        $away_duels = Duel::with('challenger:id,name')
            ->where('opponent_id', $active_character)
            ->where('status', Status::OPEN)
            ->paginate(5, ['challenger_id']);

        // get the character's completed duels
        $completed_duels = Duel::with(['challenger:id,name', 'opponent:id,name'])
            ->where(function($query) use ($active_character) {
                $query->where('challenger_id', $active_character)
                    ->orWhere('opponent_id', $active_character);
            })
            ->where('status', Status::COMPLETED)
            ->paginate(5, ['challenger_id', 'opponent_id']);

        return inertia('Duels/Index', [
            'home_duels' => fn() => DuelResource::collection($home_duels),
            'away_duels' => fn() => DuelResource::collection($away_duels),
            'completed_duels' => fn() => DuelResource::collection($completed_duels),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // get the current player's characters count
        $count = $this->countActiveCharacters(auth()->user()->id);

        // if the player doesn't have any characters
        if ($count === 0) {

            // display an error instead
            return inertia('Duels/NoActiveCharacters');
        }

        // get offensive moves
        $offensive_moves = Move::where('move_type', MoveType::OFFENSIVE)->get(['id', 'name']);

        // get defensive moves
        $defensive_moves = Move::where('move_type', MoveType::DEFENSIVE)->get(['id', 'name']);

        // return inertia response
        return inertia('Duels/Create', [
            'offensive_moves' => fn() => MoveResource::collection($offensive_moves),
            'defensive_moves' => fn() => MoveResource::collection($defensive_moves),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDuelRequest $request)
    {
        // retrieve only the opponent, offensive and defensive moves from the request
        $validated = $request->safe()->only(['opponent',
            'offensive_move1',
            'offensive_move2',
            'offensive_move3',
            'offensive_move4',
            'offensive_move5',
            'offensive_move6',
            'defensive_move1',
            'defensive_move2',
            'defensive_move3',
            'defensive_move4',
            'defensive_move5',
            'defensive_move6'
        ]);

        // get the players active character
        $challenger_active_character = Character::where('user_id', auth()->user()->id)
            ->where('active', 1)
            ->first(['id']);

        // get the opponents character id
        $opponent_active_character = Character::where('name', $validated['opponent'])
            ->first(['id']);

        // create the match
        $duel = Duel::create([
            'challenger_id' => $challenger_active_character->id,
            'opponent_id' => $opponent_active_character->id,
            'status' => Status::OPEN,
        ]);

        // Filter fields that start with 'offensive_move' and get their ids
        $offensive_moves = array_values(array_filter($validated, function ($key) {
            return str_starts_with($key, 'offensive_move');
        }, ARRAY_FILTER_USE_KEY));

        // counter to hold current move order
        $offensive_move_order = 0;

        // Iterate over each offensive move
        foreach ($offensive_moves as $move_id) {
            // create a duel action for each offensive move the user has picked
            DuelAction::create([
                'duel_id' => $duel->id,
                'move_id' => $move_id,
                'move_type' => MoveType::OFFENSIVE,
                'move_actor' => MoveActor::CHALLENGER,
                'order' => $offensive_move_order,
            ]);
            $offensive_move_order++;
        }

        // Filter fields that start with 'defensive_move' and get their ids
        $defensive_moves = array_values(array_filter($validated, function ($key) {
            return str_starts_with($key, 'defensive_move');
        }, ARRAY_FILTER_USE_KEY));

        // counter to hold current move order
        $defensive_move_order = 0;

        // Iterate over each defensive move
        foreach ($defensive_moves as $move_id) {
            // create a duel action for each defensive move the user has picked
            DuelAction::create([
                'duel_id' => $duel->id,
                'move_id' => $move_id,
                'move_type' => MoveType::DEFENSIVE,
                'move_actor' => MoveActor::CHALLENGER,
                'order' => $defensive_move_order,
            ]);
            $defensive_move_order++;
        }

        // then redirect back to the duel index
        return redirect('duels');
    }

    /**
     * Display the specified resource.
     */
    public function show(duel $duel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(duel $duel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, duel $duel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(duel $duel)
    {
        //
    }

    /**
     * Returns the total number of active characters the user has
     * @param int $id user id
     * @return int number of active characters user has
     */
    private function countActiveCharacters(int $id) : int
    {
        // return the players active character count
        return Character::select('id')
            ->where('user_id', $id)
            ->where('active', true)
            ->count();
    }
}
