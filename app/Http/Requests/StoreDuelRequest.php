<?php

namespace App\Http\Requests;

use App\Rules\NotOwnCharacters;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreDuelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            // validate the input
            'opponent' => [
                'alpha_num:ascii',
                'required',
                'min:4',
                'max:15',
                'exists:\App\Models\Character,name',
                new NotOwnCharacters()
            ],
            'offensive_move1' => 'exists:\App\Models\Move,id',
            'offensive_move2' => 'exists:\App\Models\Move,id',
            'offensive_move3' => 'exists:\App\Models\Move,id',
            'offensive_move4' => 'exists:\App\Models\Move,id',
            'offensive_move5' => 'exists:\App\Models\Move,id',
            'offensive_move6' => 'exists:\App\Models\Move,id',
            'defensive_move1' => 'exists:\App\Models\Move,id',
            'defensive_move2' => 'exists:\App\Models\Move,id',
            'defensive_move3' => 'exists:\App\Models\Move,id',
            'defensive_move4' => 'exists:\App\Models\Move,id',
            'defensive_move5' => 'exists:\App\Models\Move,id',
            'defensive_move6' => 'exists:\App\Models\Move,id',
        ];
    }
}
