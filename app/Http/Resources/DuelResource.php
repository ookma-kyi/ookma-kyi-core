<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DuelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'challenger' => CharacterResource::make($this->whenLoaded('challenger')),
            'opponent' => CharacterResource::make($this->whenLoaded('opponent')),
            'status' => $this->status,
        ];
    }
}
