<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Duel extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'challenger_id',
        'opponent_id',
        'status'
    ];

    public function challenger(): BelongsTo
    {
        return $this->belongsTo(Character::class, 'challenger_id');
    }

    public function opponent(): BelongsTo
    {
        return $this->belongsTo(Character::class, 'opponent_id');
    }
}
