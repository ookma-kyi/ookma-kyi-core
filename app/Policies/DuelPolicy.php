<?php

namespace App\Policies;

use App\Models\Duel;
use App\Models\User;
use Filament\Facades\Filament;

class DuelPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Duel $duel): bool
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Duel $duel): bool
    {
        if (Filament::getCurrentPanel() && $user->hasRole('admin')) {
            return true;
        }

        if ($user->id === $duel->opponent->user->id) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Duel $duel): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Duel $duel): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Duel $duel): bool
    {
        return false;
    }
}
