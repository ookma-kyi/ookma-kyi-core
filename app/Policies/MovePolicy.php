<?php

namespace App\Policies;

use App\Models\Move;
use App\Models\User;

class MovePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        // only staff can view the move models admin panel
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Move $move): bool
    {
        // anyone can view all moves
        return true;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        // only staff can create moves
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Move $move): bool
    {
        // only admin can alter any moves
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Move $move): bool
    {
        // only admins can delete any moves
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the model.
     * UNUSED
     */
    public function restore(User $user, Move $move): bool
    {
        // only admin can restore moves
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the model.
     * UNUSED
     */
    public function forceDelete(User $user, Move $move): bool
    {
        // only admin can force delete moves
        return $user->hasRole('admin');
    }
}
