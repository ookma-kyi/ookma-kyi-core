<?php

namespace App\Policies;

use App\Models\Rank;
use App\Models\User;

class RankPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        // only admin can view the rank models admin panel
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Rank $rank): bool
    {
        // anyone can view all ranks
        return true;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        // only admin can create ranks
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Rank $rank): bool
    {
        // only admin can alter any ranks
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Rank $rank): bool
    {
        // only admins can delete any ranks
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the model.
     * UNUSED
     */
    public function restore(User $user, Rank $rank): bool
    {
        // only admin can restore ranks
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the model.
     * UNUSED
     */
    public function forceDelete(User $user, Rank $rank): bool
    {
        // only admin can force delete ranks
        return $user->hasRole('admin');
    }
}
