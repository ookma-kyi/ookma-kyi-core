<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;

class RolePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        // only admins can view the role models admin panel
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Role $role): bool
    {
        // anyone can view a role
        return true;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        // only admins can create roles
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Role $role): bool
    {
        // only admins can alter any roles
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Role $role): bool
    {
        // only admins can delete any roles
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can restore the model.
     * UNUSED
     */
    public function restore(User $user, Role $role): bool
    {
        // only admins can restore roles
        return $user->hasRole('admin');
    }

    /**
     * Determine whether the user can permanently delete the model.
     * UNUSED
     */
    public function forceDelete(User $user, Role $role): bool
    {
        // only admins can force delete roles
        return $user->hasRole('admin');
    }
}
