<?php

namespace App\Rules;

use App\Models\Character;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Translation\PotentiallyTranslatedString;

class NotOwnCharacters implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param Closure(string, ?string=): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        // get all the user's characters
        $characters = Character::where('user_id', Auth::user()->id)->get(['id', 'name']);
        foreach ($characters as $character) {
            if($value === $character->name) {
                $fail('You cannot fight yourself!');
            }
        }
    }
}
