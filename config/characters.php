<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Max Characters allowed per user
    |--------------------------------------------------------------------------
    |
    | This value is maximum number of characters a single user may have at
    | once. This value is used when the player attempts to create a new
    | character. If the limit is reached the player won't be allowed to
    | create any more characters until one is deleted by the user. Set this
    | to the maximum number of characters you would like to allow or 0 if you
    | would like to block character creation.
    |
    */

    'max_allowed' => env('CHARACTERS_MAX_ALLOWED', 1),

];
