<?php

namespace Database\Factories;

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Log;

/**
 * @extends Factory<Character>
 */
class CharacterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $animals = [
            'badger', 'bat', 'bison', 'bull', 'bunny',
            'cat', 'crab', 'chipmunk', 'doe', 'dog',
            'dugong', 'elephant', 'fish', 'frog', 'fox',
            'gopher', 'gorilla', 'hamster', 'hedgehog', 'kangaroo',
            'hog', 'lizard', 'llama', 'lion', 'linx',
            'mandrill', 'monkey', 'mouse', 'mustang', 'panda',
            'panther', 'parrot', 'rhino', 'seal', 'skunk',
            'snake', 'squirrel', 'tiger', 'toad', 'turtle',
            'walrus', 'warthog'
        ];

        $minXp = Rank::min('min_xp') ?? 0;
        $maxXp = Rank::max('max_xp') ?? 50;

        $xp = fake()->numberBetween($minXp , $maxXp);

        $rank = Rank::where('min_xp', '<=', $xp)->orderBy('min_xp', 'desc')->first();

        return [
            'user_id' => User::factory(),
            'active' => fake()->boolean(),
            'name' => fake()->randomElement($animals) . fake()->randomDigit(),
            'xp' => $xp,
            'rank_id' => $rank->id,
            'wins' => fake()->numberBetween(0, 99),
            'loses' => fake()->numberBetween(0, 99),
            'draws' => fake()->numberBetween(0, 99)
        ];
    }
}
