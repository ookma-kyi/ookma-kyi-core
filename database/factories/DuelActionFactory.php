<?php

namespace Database\Factories;

use App\Enumerations\MoveActor;
use App\Models\Duel;
use App\Models\DuelAction;
use App\Models\Move;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<DuelAction>
 */
class DuelActionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'duel_id' => Duel::query()->inRandomOrder()->value('id'),
            'move_id' => Move::query()->inRandomOrder()->value('id'),
            'move_actor' => MoveActor::CHALLENGER, // default to challenger
            'order' => 1, // default order, will be set in sequence
        ];
    }

    /**
     * Configure the factory to create a specific type of move.
     *
     * @param int $type
     * @return static
     */
    public function type(int $type): static
    {
        return $this->state(function (array $attributes) use ($type) {
            return [
                'move_id' => Move::where('move_type', $type)->inRandomOrder()->value('id'),
            ];
        });
    }

    /**
     * Set the move actor to challenger.
     *
     * @return static
     */
    public function challenger(): static
    {
        return $this->state([
            'move_actor' => 0,
        ]);
    }

    /**
     * Set the move actor to opponent.
     *
     * @return static
     */
    public function opponent(): static
    {
        return $this->state([
            'move_actor' => 1,
        ]);
    }

    /**
     * Set the order of the move.
     *
     * @param int $order
     * @return static
     */
    public function order(int $order): static
    {
        return $this->state([
            'order' => $order,
        ]);
    }
}
