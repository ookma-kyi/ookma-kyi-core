<?php

namespace Database\Factories;

use App\Enumerations\Status;
use App\Models\Character;
use App\Models\Duel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Duel>
 */
class DuelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'challenger_id' => Character::factory(),
            'opponent_id' => Character::factory(),
            'status' => fake()->randomElement(Status::class),
        ];
    }
}
