<?php

namespace Database\Factories;

use App\Enumerations\MoveType;
use App\Models\Move;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;

/**
 * @extends Factory<Move>
 */
class MoveFactory extends Factory
{
    // static variable to keep track if this is the 1st rank instance seeded
    protected static int $offensiveCounter = 0;

    // Static variables to keep track of move order for each type
    protected static int $defensiveCounter = 0;

    protected static array $adjectives = [
        'brutal', 'cruel', 'efficient', 'great', 'heavy',
        'mighty', 'powerful', 'quick', 'rapid', 'strong',
        'wicked'
    ];

    // Random offensive move array
    protected static array $randomOffensiveMoves = [
        'back fist', 'elbow strike', 'hook punch', 'jab', 'liver punch',
        'overhand punch', 'palm strike', 'ridge hand strike', 'spear hand', 'tiger claw',
        'uppercut'
    ];

    // Random defensive move array
    protected static array $randomDefensiveMoves = [
        'augmented block', 'cross block', 'deflecting block', 'inside block', 'knife hand block',
        'leg block', 'mountain block', 'outside block', 'palm block', 'reinforced block',
        'shin block'
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => '',   // Will be set in offensive() or defensive()
            'order' => 0,   // will be set in offensive() or defensive()
            'move_type' => MoveType::OFFENSIVE->value, // default, will be overridden if necessary
        ];
    }

    // Method to reset the instance
    public static function resetInstance(): void
    {
        self::$offensiveCounter = 0;
        self::$defensiveCounter = 0;
    }

    public function offensive(): MoveFactory|Factory
    {
        return $this->state(function (array $attributes) {
            $adjective = $this->faker->randomElement(self::$adjectives);
            $name = $adjective . ' ' . $this->faker->randomElement(self::$randomOffensiveMoves);
            Log::info("Creating Offensive Move: $name, Order: " . self::$offensiveCounter);
            return [
                'name' => $name,
                'order' => self::$offensiveCounter++,
                'move_type' => MoveType::OFFENSIVE->value,
            ];
        });
    }

    public function defensive(): MoveFactory|Factory
    {
        return $this->state(function (array $attributes) {
            $adjective = $this->faker->randomElement(self::$adjectives);
            $name = $adjective . ' ' . $this->faker->randomElement(self::$randomDefensiveMoves);
            Log::info("Creating Defensive Move: $name, Order: " . self::$defensiveCounter);
            return [
                'name' => $name,
                'order' => self::$defensiveCounter++,
                'move_type' => MoveType::DEFENSIVE->value,
            ];
        });
    }
}
