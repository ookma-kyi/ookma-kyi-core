<?php

namespace Database\Factories;

use App\Models\Rank;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Rank>
 */
class RankFactory extends Factory
{
    // static variable to keep track if this is the 1st rank instance seeded
    protected static bool $firstInstance = true;

    // Static variable to keep track of the last max XP
    protected static int $lastMaxXp = 0;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // random element array
        $randomElements = ['Amazing', 'Devine', 'Exquisite', 'Great', 'Marvelous', 'Radiant', 'Shining', 'Vibrant'];

        // generate a random element for the rank
        $randomElement = fake()->randomElements($randomElements);

        // generate a random color for the rank
        $colorName = fake()->colorName();

        // generate the name using the random element and color name
        $name = $randomElement[0] . " " . $colorName;

        // generate url safe image name using the random element and color name
        $image_url = $randomElement[0] . "_" . $colorName;

        // if this is the first rank instance
        if(self::$firstInstance) {
            // Set the minimum XP to be 0
            $minXp = 0;

            // and set firstInstance to false for the next instance
            self::$firstInstance = false;
        } else {
            // Otherwise set the minimum XP to be one more than the last max XP
            $minXp = self::$lastMaxXp + 1;
        }

        // Generate a max XP that is greater than the min XP
        // For example, you can use a random number between min XP + 20 and min XP + 50
        $maxXp = $minXp + fake()->numberBetween(20, 50);

        // Update the last max XP for the next rank
        self::$lastMaxXp = $maxXp;

        return [
            'name' => $name,
            'image' => storage_path('app/public/ranks/' . $image_url . '.png'),
            'min_xp' => $minXp,
            'max_xp' => $maxXp,
        ];
    }
}
