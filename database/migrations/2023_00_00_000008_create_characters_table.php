<?php

use App\Models\Rank;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class)->constrained();
            $table->boolean("active")->default(false);
            $table->string("name", 15);
            $table->unsignedInteger("xp")->default(0);
            $table->foreignIdFor(Rank::class)->constrained();
            $table->unsignedMediumInteger("wins")->default(0);
            $table->unsignedMediumInteger("loses")->default(0);
            $table->unsignedMediumInteger("draws")->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('characters');
    }
};
