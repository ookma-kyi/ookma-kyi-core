<?php

use App\Enumerations\Status;
use App\Models\Character;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('duels', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Character::class, "challenger_id")->constrained("characters");
            $table->foreignIdFor(Character::class, "opponent_id")->constrained("characters");
            $table->tinyInteger("status");
            $table->timestamps();
        });

        // Add the CHECK constraint
        DB::statement('ALTER TABLE duels ADD CONSTRAINT status_is_valid_range CHECK (status IN (' . implode(', ', array_map(fn($status) => $status->value, Status::cases())) . '))');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('duels');
    }
};
