<?php

use App\Enumerations\MoveType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('moves', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->unsignedTinyInteger('order');
            $table->tinyInteger('move_type')->default(MoveType::OFFENSIVE->value);
            $table->timestamps();
        });

        // Add the CHECK constraint
        DB::statement('ALTER TABLE moves ADD CONSTRAINT moves_is_valid_type CHECK (move_type IN (' . implode(', ', array_map(fn($move_type) => $move_type->value, MoveType::cases())) . '))');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('moves');
    }
};
