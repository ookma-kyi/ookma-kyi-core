<?php

use App\Enumerations\MoveActor;
use App\Enumerations\MoveType;
use App\Models\Duel;
use App\Models\Move;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('duel_actions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Duel::class)->constrained();
            $table->foreignIdFor(Move::class)->constrained();
            $table->tinyInteger('move_type');
            $table->tinyInteger('move_actor')->default(MoveActor::CHALLENGER->value);
            $table->unsignedTinyInteger('order');
            $table->timestamps();
        });

        // Add the CHECK constraint to constrain the column to a valid move type
        DB::statement('ALTER TABLE duel_actions ADD CONSTRAINT move_is_valid_type CHECK (move_type IN (' . implode(', ', array_map(fn($move_type) => $move_type->value, MoveType::cases())) . '))');

        // Add the CHECK constraint to constrain the column to a valid actor type
        DB::statement('ALTER TABLE duel_actions ADD CONSTRAINT actor_is_valid_type CHECK (move_actor IN (' . implode(', ', array_map(fn($move_actor) => $move_actor->value, MoveActor::cases())) . '))');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('duel_actions');
    }
};
