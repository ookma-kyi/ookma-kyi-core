<?php

namespace Database\Seeders;

use App\Models\Character;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CharacterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // generate a random password
        $password = Str::random(8);

        // hash the generated password
        $hashed_random_password = Hash::make($password);

        $user = User::firstOrCreate(
            ['email' => 'admin@localhost'],
            [
                'name' => 'Admin User',
                'email' => 'admin@localhost',
                'password' => $hashed_random_password,
                'email_verified_at' => '1970-01-01 00:00:00'
            ]);

        if ($user->wasRecentlyCreated) {
            // display the password for the created administrator user
            echo "*******************************************\n";
            echo "Your administrator password is: ${password}\n";
            echo "*******************************************\n";
        }

        Character::factory()->
        recycle($user)->
        create([
            'active' => 1,
            'name' => 'admin',
            'xp' => 0,
            'wins' => 0,
            'loses' => 0,
            'draws' => 0
        ]);

        // get users who aren't the admin account
        $users = User::get('id');

        // for each other seeded user account
        foreach ($users as $user) {
            // if the user doesn't have the admin role
            if(!$user->hasRole('admin')) {
                // create a single character each
                Character::factory()->recycle($user)->create();
            }
        }
    }
}
