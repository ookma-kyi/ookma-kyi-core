<?php

namespace Database\Seeders;

use App\Models\Move;
use Database\Factories\MoveFactory;
use Illuminate\Database\Seeder;

class MoveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // reset the counter
        MoveFactory::resetInstance();

        // Create offensive moves
        Move::factory()->count(3)->offensive()->create();

        // reset the counter
        MoveFactory::resetInstance();

        // Create corresponding defensive move
        Move::factory()->count(3)->defensive()->create();
    }
}
