<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create the admin role
        $adminRole = Role::create(['name' => 'admin']);

        // create the moderator role
        Role::create(['name' => 'moderator']);

        //  give all permissions to the admin role
        $adminRole->givePermissionTo(Permission::all());
    }
}
