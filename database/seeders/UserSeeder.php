<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // generate a random password
        $password = Str::random(8);

        // display the password for the administrator user
        echo "*******************************************\n";
        echo "Your administrator password is: ${password}\n";
        echo "*******************************************\n";

        // hash the generated password
        $hashed_random_password = Hash::make($password);

        // create the administrator user
        $user = User::factory()->create([
            'name' => 'Admin',     // default account
            'email' => 'admin@localhost',
            'password' => $hashed_random_password,
            'email_verified_at' => '1970-01-01 00:00:00'
        ]);

        // find the role or create it if it doesn't exist
        $admin_role = Role::findOrCreate('admin');

        // if the role was just created
        if ($admin_role->wasRecentlyCreated) {
            //  give all permissions to the admin role
            $admin_role->givePermissionTo(Permission::all());
        }

        $user->assignRole('admin');

        // generate an additional 4 user accounts
        User::factory(4)->create([
            'email_verified_at' => '1970-01-01 00:00:00'
        ]);
    }
}
