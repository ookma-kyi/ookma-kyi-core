
# Privacy Policy

We will not, under any circumstances whatsoever, give out or sell your information (including email address) to
anyone.

We will also never email you unless it is important and directly related to your website experience (lost username,
password reset etc.).

Also, no one will ever have access to your personal information and other data. The only exceptions to this rule:

From time to time, we will perform system updates, routine maintenance, and patch bugs and/or exploits. However,
all your data will still remain 100% confidential. It's possible that we will be required by subpoena or other legal
action to grant access to an individual in the process of an investigation.
