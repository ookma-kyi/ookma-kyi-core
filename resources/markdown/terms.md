# Terms of Service

These Terms of Service ("TOS") may be altered at any time. Such changes will be presented at log-on; however,
each player should at all times be aware of their obligations pursuant to the TOS at all times. Failure to comply
with the TOS may result in the immediate deletion of the player's account.

## AGE REQUIREMENTS

This game is not intended for persons under 13 years of age. Some content may be inappropriate for young children. By
joining, you are stating that you are thirteen years or older.

## INTELLECTUAL RIGHTS

Any individual who attempts to reverse, engineer, hack, gain unauthorized entry into or undermine the system
integrity of this site (including attempting to access any internal part of the game from an offsite location) will
have their account(s) removed from game play and any future accounts closed. Such incidents will be legally followed
through with the fullest extent of the law.

## TERMINATING YOUR ACCOUNT

Individuals who no longer wish to play the game may terminate their accounts by not logging into their accounts.

## USER GENERATED CONTENT

To the extent that portions of this site provide users an opportunity to post and/or exchange information and/or
ideas, and opinions ("postings") in chat rooms and message boards and via private mail, please be advised
that postings do not necessarily reflect the views of the administrators of this site. Although we may periodically
monitor exchanged and posted information, in no event do we assume or have any responsibility or liability for any
postings or any claims, damages or losses resulting from their use and/or appearance on or in conjunction with this
site or elsewhere. Players are solely responsible for the content of their messages and postings; however, we
reserve the right to edit, delete or refuse to post any postings that violate these TOS as well as revoke the
privileges of any player who does not comply with the TOS.

## OPERATIONAL INTERFERENCES

The players agree to not, through use of software or other means, interfere with site operation and page content. Any
automated software written to interact with the game is strictly forbidden. Players agree to follow the
administrated requests and instructions. Interference with this site and or failure to follow administrated requests
and instructions may result in the immediate deletion of the player's account.

## PAYMENTS ETC.

Once a charge is made to an account, that charge cannot be switched to another account. Make sure all orders are
placed to the proper account.

## FORCE MAJEURE

The site administrator shall not be responsible for any failure to perform its obligations under this Agreement,
connectivity, loss of account information, if such failure is caused by events or conditions beyond the website
administrators reasonable control.

## JURISDICTION

Any dispute arising in connection with the validity, interpretation, implementation or performance of the TOS shall
be at the exclusive jurisdiction of the Courts of the United States. The TOS are exclusively governed by the laws of the
United States.

## ADDENDUM

Other sections of the game may have its own rules such as the chat rooms. These rules are in addition to the TOS and
do not replace the TOS. Violation of these rules is considered to be a violation of the TOS and will be dealt with
accordingly. Players should familiarize themselves with all game rules before playing. Failure to be familiar with
the game rules and/or TOS is not an excuse for breaking them and will not be accepted as a defense of your
account.

THIS SITE AND ALL MATERIALS CONTAINED ON IT ARE DISTRIBUTED AND TRANSMITTED ON AN "AS IS" AND "AS
AVAILABLE" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE
UNDER APPLICABLE LAW, THIS SITE ADMINISTRATORS DISCLAIMS ALL WARRANTIES EXPRESS OR IMPLIED, INCLUDING WITHOUT
LIMITATION, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR SERVICE. THE SITE ADMINISTRATORS DOES NOT
WARRANT THAT THE FUNCTIONS CONTAINED IN THIS SITE OR MATERIALS WILL BE UNINTERRUPTED OR ERROR FREE, THAT DEFECTS
WILL BE CORRECTED, OR THAT THIS SITE OR THE SERVICE THAT MAKE IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL
COMPONENTS. THE SITE ADMINISTRATORS DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF OR THE RESULTS
OF THE USE OF THE MATERIALS IN THIS SITE WITH THE REGARD TO CORRECTNESS, ACCURACY, RELIABILITY OR OTHERWISE. THE
ENTIRE RISK AS TO THE QUALITY, ACCURACY, ADEQUACY, COMPLETENESS, CORRECTNESS AND VALIDITY OF ANY MATERIAL REST WITH
YOU THE PLAYER. THE PLAYER ASSUMES THE COMPLETE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. TO THE
FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, THE SITE ADMINISTRATORS, ITS AFFILIATES AND THEIR RESPECTIVE
OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, LICENSORS, REPRESENTATIVES AND THIRD PARTY PROVIDERS TO THE SITE WILL NOT BE
LIABLE FOR DAMAGES OF ANY KIND INCLUDING WITHOUT LIMITATION, COMPENSATORY, CONSEQUENTIAL, INCIDENTAL, INDIRECT,
SPECIAL OR SIMILAR DAMAGES, THAT MAY RESULT IN THE USE OF OR THE INABILITY TO USE, THE MATERIALS CONTAINED ON THIS
SITE, WHETHER THE MATERIAL IS PROVIDED OR OTHERWISE SUPPLIED BY THE SITE ADMINISTRATORS OR ANY THIRD PARTY.
NOTWITHSTANDING THE FOREGOING, IN NO EVENT SHALL THE SITE ADMINISTRATORS HAVE ANY LIABILITY TO ANY PLAYER OR THIRD
PARTY FOR ANY CLAIMS, DAMAGES, LOSSES, AND OR CAUSES OF ACTION (WHETHER IN CONTRACT, TORT OR OTHERWISE) EXCEEDING
THE AMOUNT PAID BY YOU, IF ANY, FOR ACCESSING THIS SITE. USE OF THIS SITE CONSTITUTES AGREEMENT WITH THE CURRENT
TERMS AND CONDITIONS.
