<?php

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;
use Inertia\Testing\AssertableInertia;

use function Pest\Laravel\get;

it('gives back successful redirect response for unauthenticated users for the characters.create page', function () {
    $user = null;
    get(route('characters.create', ['user' => $user]))
        ->assertStatus(302);
});

it('can be accessed by verified user', function () {
    // Arrange
    $user = User::factory()->create();

    // Act & Assert
    $this->actingAs($user)
        ->get(route('characters.create'))
        ->assertOk();
});

it('Should return the correct component', function () {
   // Arrange
   $user = User::factory()->create();

   $this->actingAs($user)->
   get(route('characters.create'))
       ->assertInertia(fn (AssertableInertia $inertia) => $inertia
           ->component('Characters/Create', true)
       );
});

it('Should allow the correct number of characters', function () {
    // Arrange
    $user = User::factory()->create();
    $rank = Rank::factory(6)->create();

    $counter = config("characters.max_allowed") + 1;

    Character::factory($counter)->recycle($user, $rank)->create();

    $this->actingAs($user)->
    get(route('characters.create'))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->component('Characters/MaxCharacters', true)
        );
});

?>
