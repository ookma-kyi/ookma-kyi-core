<?php

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;

it('it requires authentication for the characters.delete page', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create();

    $this->delete(route('characters.destroy', $character))
        ->assertRedirect(route('login'));
});

it('it deletes a character', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create(['active' => 0]);

    // Act & Assert
    $this->actingAs($character->user)
        ->delete(route('characters.destroy', $character));

    $this->assertSoftDeleted($character);
});

it('cannot update a character from another user', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create();

    // Act & Assert
    $this->actingAs(User::factory()->create())
        ->delete(route('characters.destroy', $character))
        ->assertForbidden();
});

?>
