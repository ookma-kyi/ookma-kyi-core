<?php

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;
use Inertia\Testing\AssertableInertia;

use function Pest\Laravel\get;

it('gives back successful redirect response for unauthenticated users for the tasks.show page', function () {
    $user = null;
    get(route('characters.index', ['user' => $user]))
        ->assertStatus(302);
});

it('can be accessed by verified user', function () {
    // Arrange
    $user = User::factory()->create();

    // Act & Assert
    $this->actingAs($user)
        ->get(route('characters.index'))
        ->assertOk();
});

it('Should return the correct component', function () {
   // Arrange
   $user = User::factory()->create();

   $this->actingAs($user)->
   get(route('characters.index'))
       ->assertInertia(fn (AssertableInertia $inertia) => $inertia
           ->component('Characters/Index', true)
       );
});

it('passes characters to the view', function () {
    $user = User::factory()->create();
    // Seed the database with 6 rank instances
    Rank::factory(6)->create();
    Character::factory()->recycle($user)->create();

    $this->actingAs($user)->
    get(route('characters.index', ['user' => $user]))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->has('characters')
        );
});

?>
