<?php

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;

use function Pest\Laravel\get;

beforeEach(function () {
    $this->validData = fn() => [
            'name' => 'Test',
        ];
});

it('gives back successful redirect response for unauthenticated users for the characters.store page', function () {
    get(route('characters.store'))->assertRedirect(route('login'));
});

it('can be accessed by verified user', function () {
    // Arrange
    $user = User::factory()->create();

    // Act & Assert
    $this->actingAs($user)
        ->get(route('characters.store'))
        ->assertOk();
});

it('Stores a character', function () {
    // Arrange
    $user = User::factory()->create();
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $data = value($this->validData);

    // Act & Assert
    $this->actingAs($user)->post(route('characters.store'), $data);

    $this->assertDatabaseHas(Character::class, [
        ...$data,
        'user_id' => $user->id,
    ]);
});

it('redirects to the character index page', function () {
    // arrange
    $user = User::factory()->create();
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    // Act & Assert
    $this->actingAs($user)->
    post(route('characters.store'), value($this->validData))
    ->assertRedirect('characters');
});

it('requires valid data', function (array $badData, array|string $errors) {

    // Act & Assert
    $this->actingAs(User::factory()->create())
        ->post(route('characters.store'), [...value($this->validData), ...$badData])
        ->assertInvalid($errors);
})->with([
    [['name' => null], 'name'],
    [['name' => true], 'name'],
    [['name' => 1], 'name'],
    [['name' => 1.5], 'name'],
    [['name' => '!!!!'], 'name'],
    [['name' => '    '], 'name'],
    [['name' => str_repeat('a', 16)], 'name'],
    [['name' => str_repeat('a', 2)], 'name'],
]);

?>
