<?php

use App\Models\Rank;
use App\Models\Character;
use App\Models\User;

use function Pest\Laravel\get;

it('it requires authentication for the characters.update page', function () {
    get(route('characters.store'))->assertRedirect(route('login'));
});

it('it updates a character', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create(['active' => 0]);

    // Act & Assert
    $this->actingAs($character->user)
        ->put(route('characters.update', $character), ['active' => 1]);

    $this->assertDatabaseHas(Character::class, [
        'id' => $character->id,
        'active' => 1,
    ]);
});

it('cannot update a character from another user', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create();

    // Act & Assert
    $this->actingAs(User::factory()->create())
        ->put(route('characters.update', ['character' => $character]), ['active' => 1])
        ->assertForbidden();
});

it('sets active character and redirects', function () {
    // Arrange
    Rank::factory()->create([
        'min_xp' => 0,
    ]);

    $character = Character::factory()->create(['active' => 0]);

    $this->actingAs($character->user)
        ->put(route('characters.update', ['character' => $character]), ['active' => 1])
        ->assertOk();
});

?>
