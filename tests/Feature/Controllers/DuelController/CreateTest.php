<?php

use App\Models\Character;
use App\Models\Rank;
use App\Models\User;
use Inertia\Testing\AssertableInertia;

use function Pest\Laravel\get;

it('gives back successful redirect response for unauthenticated users for the duels.create page', function () {
    $user = null;
    get(route('duels.create', ['user' => $user]))
        ->assertStatus(302);
});

it('can be accessed by verified user', function () {
    // Arrange
    $user = User::factory()->create();

    // Act & Assert
    $this->actingAs($user)
        ->get(route('duels.create'))
        ->assertOk();
});

it('Should return the correct component', function () {
   // Arrange
   $user = User::factory()->create();
    // seed the database with 6 rank instances
    Rank::factory(6)->create();
    //create a character for the user
    Character::factory()->recycle($user)->create();

   $this->actingAs($user)->
   get(route('duels.create'))
       ->assertInertia(fn (AssertableInertia $inertia) => $inertia
           ->component('Duels/Create', true)
       );
});

it('Should return no characters component', function () {
    // Arrange
    $user = User::factory()->create();

    $this->actingAs($user)->
    get(route('duels.create'))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->component('Duels/NoCharacters', true)
        );
});

?>
