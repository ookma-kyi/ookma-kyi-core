<?php

use App\Enumerations\Status;
use App\Models\Rank;
use App\Models\Character;
use App\Models\Duel;
use App\Models\User;
use Inertia\Testing\AssertableInertia;

use function Pest\Laravel\get;

it('gives back successful redirect response for unauthenticated users for the duels.index page', function () {
    $user = null;
    get(route('duels.index', ['user' => $user]))
        ->assertStatus(302);
});

it('can be accessed by verified user', function () {
    // Arrange
    $user = User::factory()->create();

    // Act & Assert
    $this->actingAs($user)
        ->get(route('duels.index'))
        ->assertOk();
});

it('Should return the correct component', function () {
   // Arrange
   $user = User::factory()->create();
   // Seed the database with 6 rank instances
   Rank::factory(6)->create();
   Character::factory()->recycle($user)->create();

   $this->actingAs($user)->
   get(route('duels.index'))
       ->assertInertia(fn (AssertableInertia $inertia) => $inertia
           ->component('Duels/Index', true)
       );
});

it('Should return no characters component', function () {
    // Arrange
    $user = User::factory()->create();

    $this->actingAs($user)->
    get(route('duels.index'))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->component('Duels/NoCharacters', true)
        );
});

it('passes home duels to the view', function () {
    //create instance of the user
    $user = User::factory()->create();
    // seed the database with 6 rank instances
    Rank::factory(6)->create();
    //create a character for the user
    $character = Character::factory()->recycle($user)->create();
    // Seed the database with 6 duel instances
    Duel::factory(5)->create([
        'challenger_id' => $character
    ]);

    $this->actingAs($user)->
    get(route('duels.index', ['user' => $user]))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->has('home_duels')
        );
});

it('passes away duels to the view', function () {
    //create instance of the user
    $user = User::factory()->create();
    // seed the database with 6 rank instances
    Rank::factory(6)->create();
    //create a character for the user
    $character = Character::factory()->recycle($user)->create();
    // Seed the database with 6 duel instances
    Duel::factory(5)->create([
        'opponent_id' => $character
    ]);

    $this->actingAs($user)->
    get(route('duels.index', ['user' => $user]))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->has('away_duels')
        );
});

it('passes completed duels to the view', function () {
    //create instance of the user
    $user = User::factory()->create();
    // seed the database with 6 rank instances
    Rank::factory(6)->create();
    //create a character for the user
    $character = Character::factory()->recycle($user)->create();
    // Seed the database with 6 duel instances
    Duel::factory(5)->create([
        'challenger_id' => $character,
        'status' => Status::COMPLETED
    ]);

    $this->actingAs($user)->
    get(route('duels.index', ['user' => $user]))
        ->assertInertia(fn (AssertableInertia $inertia) => $inertia
            ->has('completed_duels')
        );
});

?>
